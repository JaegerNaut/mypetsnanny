<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddService3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumns('landing', ['service3_title', 'service3_image', 'service3_text'])) {
            Schema::table('landing', function (Blueprint $table) {
                $table->string('service3_title');
                $table->string('service3_image');
                $table->text('service3_text');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // ...
    }
}
